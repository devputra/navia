//
//  BaseViewController.swift
//  NaviaDoctors
//
//  Created by Shubham Sharma on 15/09/18.
//  Copyright © 2018 Shubham Sharma. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Shows an alert with given title, message and cancel button.
     - Parameter title: Title of the alert.
     - Parameter message: Message to show on alert.
     - Parameter cancelButtonTitle: Title of the cancel button.
     */
    func showAlertWithTitle(_ title:String, message:String, cancelButtonTitle cancelTitle:String) {
        
        let anAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .default, handler: nil)
        anAlertController.addAction(cancelAction)
        present(anAlertController, animated: true, completion: nil)
    }

}

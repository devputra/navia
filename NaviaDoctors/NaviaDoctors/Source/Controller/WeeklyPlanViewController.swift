//
//  WeeklyPlanViewController.swift
//  NaviaDoctors
//
//  Created by Shubham Sharma on 15/09/18.
//  Copyright © 2018 Shubham Sharma. All rights reserved.
//

import UIKit
import UserNotifications

class WeeklyPlanViewController: BaseViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var reminderSwitch: UISwitch!
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet weak var planTableView: UITableView!
    
    var planDuration = 20
    var diet:[Diet]?
    
    let notificationIdentifier = "NaviaDoctors"
    let timeMargin = 5
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        planTableView.dataSource = self
        sendWebRequestForGettingDietPlan()
        
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (settings) in
            
            switch settings.authorizationStatus {
                
            case .notDetermined:
                DispatchQueue.main.async {
                    self.reminderSwitch.isEnabled = false
                }
                self.askForThePermissionOfNotification()
            case .denied:
                DispatchQueue.main.async {
                    self.reminderSwitch.isEnabled = false
                }
                self.showAlertWithTitle("Notificaion Access Required", message: "Please allow access to show the notification for meal reminder.", cancelButtonTitle: "Ok")
            case .authorized:
                DispatchQueue.main.async {
                    self.reminderSwitch.isEnabled = true
                }
            }
        }

    }
    
    // MARK: - Actions
    
    /**
     Unable or dissable the notification.
     - Parameter sender: UISwitch object that was changed by the user.
     */
    @IBAction func reminderSettingDidChange(_ sender: UISwitch) {
        
        if sender.isOn {
            setupReminders()
        }
        else {
            removeReminders()
        }
    }
    
    // MARK: - Helpers
    
    func updatePlanWithDays(_ days:Int) {
        mainTitleLabel.text = "Weekly plan for next \(days) days"
    }
    
    /// Ask the user for granting the permission of showing notifications.
    func askForThePermissionOfNotification() {
        
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound]
        
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                DispatchQueue.main.async {
                    self.reminderSwitch.isEnabled = false
                }
                self.showAlertWithTitle("Notificaion Access Required", message: "Please allow access to show the notification for meal reminder.", cancelButtonTitle: "Ok")
            }
            else {
                DispatchQueue.main.async {
                    self.reminderSwitch.isEnabled = true
                }
                self.setupReminders()
            }
        }
    }
    
    /// Adds the reminders.
    func setupReminders() {
        
        // Get all the days till plan.
        var allDates = [Date]()
        let calendar = NSCalendar.current
        for counter in 1...planDuration {
            let newDate = calendar.date(byAdding: .day, value: counter, to: Date())!
            allDates.append(newDate)
        }
        
        // Get all the selected days till plan on which we need to show the reminder.
        var allSelectedDays = [[Date]]()
        if let plan = diet {
            for aDiet in plan {
                let days = allDates.filter { $0.dayName?.lowercased() == aDiet.dayName?.lowercased() }
                allSelectedDays.append(days)
            }
        }
        
        for similarDays in allSelectedDays {
            
            let dayName = similarDays.first?.dayName?.lowercased()
            for dietPlan in diet! {
                
                if dietPlan.dayName?.lowercased() == dayName {
                    // Prepare the reminder for each meal plan.
                    for aMeal in dietPlan.meal! {
                        
                        let content = UNMutableNotificationContent()
                        content.title = "It's time to take " + (aMeal.foodName ?? "")
                        content.sound = UNNotificationSound.default()
                        
                        for aSelectedDate in similarDays {
                            let targetDateAndTime =  aSelectedDate.newDateBySetting(hour: aMeal.time!.hour, minute:  (aMeal.time!.minute - timeMargin) , second: 0)
                            let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.year,.month,.day,.hour,.minute], from: targetDateAndTime!), repeats: false)
                            
                            let request = UNNotificationRequest(identifier: notificationIdentifier,
                                                                content: content, trigger: trigger)
                            UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
                            })
                        }
                    }
                    break
                }
            }
        }
    }
    
    /// Removes the reminders.
    func removeReminders() {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [notificationIdentifier])
    }
}

// MARK: - TableView DataSource

extension WeeklyPlanViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return diet?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diet![section].meal?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WeeklyPlanTableViewCell.self)) as! WeeklyPlanTableViewCell
        cell.setDetails(meal: diet![indexPath.section].meal![indexPath.row])
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return diet?[section].dayName
    }

}


// MARK: - Web Request Handling

extension WeeklyPlanViewController {
    
    func sendWebRequestForGettingDietPlan() {
        
        let webManager = SSWebRequestManager()
        _ = webManager.requestHTTPGET(url: "https://naviadoctors.com/dummy/", parameters: nil, finished: { (response) in
            
            if let dietDuration = response["diet_duration"] as? Int {
                self.planDuration = dietDuration
                self.updatePlanWithDays(dietDuration)
            }
            if let plan = response["week_diet_data"] as? [String:Any?] {
                self.diet = Diet.dietsFrom(info: plan)
                self.planTableView.reloadData()
            }
            
        }, failed: { (error) in
            self.showAlertWithTitle("Server unreachable", message: "Please check your internet connection.", cancelButtonTitle: "Ok")
        })
        
    }
}

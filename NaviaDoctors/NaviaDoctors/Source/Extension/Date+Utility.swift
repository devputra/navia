//
//  Date+Utility.swift
//  NaviaDoctors
//
//  Created by Shubham Sharma on 04/06/18.
//  Copyright © 2018 Shubham Sharma. All rights reserved.
//

import Foundation

extension Date {
    
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    var hour: Int {
        return Calendar.current.component(.hour, from: self)
    }
    var minute: Int {
        return Calendar.current.component(.minute, from: self)
    }
    var second: Int {
        return Calendar.current.component(.second, from: self)
    }
    
    var dayName:String? {
        return getDateStringWithFormat(format: "EEEE")
    }
    
    /**
     Takes the UTC date string and prepare the Date .
     - Parameter string: String representing the date in UTC.
     - Parameter dateFormat: Format of the UTC date string.
     - Returns: Returns Date object with UTC timezone.
     */
    static func getUTCDateFromString(_ string:String, withFormat dateFormat:String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if Date.has12HourFormat() {
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        }
        let date = dateFormatter.date(from: string)!
        return date
    }
    
    /**
     Takes the date string and prepare the Date .
     - Parameter string: String representing the date.
     - Parameter dateFormat: Format of the UTC date string.
     - Returns: Returns Date object with UTC timezone.
     */
    static func getDateFromString(_ string:String, withFormat dateFormat:String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        if Date.has12HourFormat() {
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        }
        let date = dateFormatter.date(from: string)!
        return date
    }
    
    /**
     Check whether the current time is in 12 Hour format or not.
     - Returns: Returns true if current has 12 hour format OTHERWIES false.
     */
    static func has12HourFormat() -> Bool {
        let formatStringForHours: String? = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: NSLocale.current)
        
        let hasAMPM: Bool = (formatStringForHours?.contains("a"))!
        return hasAMPM
    }
    
    /**
     Returns the date string with given format.
     - Parameter format: String representing the date format.
     - Returns: Returns the date string with given format. If format is invalid it returns nil.
     */
    func getDateStringWithFormat(format:String) -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        if Date.has12HourFormat() {
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        }
        return dateFormatter.string(from: self)
    }
    
    func newDateBySetting(hour:Int, minute:Int, second:Int) -> Date? {
        return Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: self)
    }
}


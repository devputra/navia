//
//  Diet.swift
//  NaviaDoctors
//
//  Created by Shubham Sharma on 15/09/18.
//  Copyright © 2018 Shubham Sharma. All rights reserved.
//

import UIKit

class Diet: NSObject {

    var meal:[Meal]?
    var date:Date?
    var dayName:String?
    
    override init() {
    }
    
    class func dietsFrom(info:[String:Any?]) -> [Diet] {
        
        var list = [Diet]()
        
        for someKey in info.keys {
            
            let someDiet = Diet()
            someDiet.dayName = someKey.capitalized
            
            if let mealInfo = info[someKey] as? [[String:Any?]] {
                someDiet.meal = Meal.mealListFromDataArray(mealInfo)
            }
            list.append(someDiet)
        }
        return list
    }
}

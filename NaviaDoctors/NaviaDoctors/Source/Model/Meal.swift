//
//  Meal.swift
//  NaviaDoctors
//
//  Created by Shubham Sharma on 15/09/18.
//  Copyright © 2018 Shubham Sharma. All rights reserved.
//

import UIKit

class Meal: NSObject {

    var foodName:String?
    var time:Date?
    
    override init() {
    }
    
    init(mealInfo:[String:Any?]) {
        
        foodName = mealInfo["food"] as? String
        if let time = mealInfo["meal_time"] as? String {
            self.time = Date.getDateFromString(time, withFormat: "HH:mm")
        }
    }
    
    class func mealListFromDataArray(_ dataArray:[[String:Any?]]) -> [Meal] {
        let mealList = dataArray.map { (mealInfo) -> Meal in
            Meal(mealInfo: mealInfo)
        }
        return mealList
    }
}

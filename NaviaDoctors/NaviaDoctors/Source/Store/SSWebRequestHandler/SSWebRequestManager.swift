//
//  SSWebRequestManager.swift
//  NaviaDoctors
//
//  Created by Shubham Sharma on 01/06/18.
//  Copyright © 2018 Shubham Sharma. All rights reserved.
//

import UIKit
import Alamofire

class SSWebRequestManager: NSObject {
    
    class var isNewWorkAvailable:Bool {
        get {
            return  NetworkReachabilityManager()!.isReachable
        }
    }
    
    /**
     Make a HTTP GET request and returns with either response or failure
     - Parameter url Request URL
     - Parameter parameters Request Parameters
     */
    func requestHTTPGET(url:String, parameters:Dictionary<String,Any>?, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void) -> DataRequest {
        // HTTP Get request
        let request =    Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {
                    finished(data as! Dictionary<String, Any>)
                }
                break
            case .failure(_):
                if let error = response.result.error {
                    failed(error.localizedDescription)
                }
                break
            }
        }
        return request
    }
}


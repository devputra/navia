//
//  WeeklyPlanTableViewCell.swift
//  NaviaDoctors
//
//  Created by Shubham Sharma on 15/09/18.
//  Copyright © 2018 Shubham Sharma. All rights reserved.
//

import UIKit

class WeeklyPlanTableViewCell: UITableViewCell {

    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var mealTimingLabel: UILabel!
    
    var someMeal:Meal?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setDetails(meal:Meal) {
        self.someMeal  = meal
        mealNameLabel.text = meal.foodName?.capitalized
        mealTimingLabel.text = meal.time?.getDateStringWithFormat(format: "HH:mm")
    }

}
